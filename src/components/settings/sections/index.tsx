import Appearance from "./appearance";
import About from "./about";
import Cloaking from "./cloaking";
import Search from "./search";

export default {
    Appearance,
    About,
    Cloaking,
    Search
};